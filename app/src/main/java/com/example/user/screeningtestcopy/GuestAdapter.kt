package com.example.user.screeningtestcopy

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.example.user.screeningtestcopy.Model.Guest
import kotlinx.android.synthetic.main.guest_list.view.*

class GuestAdapter (var context: Context) : RecyclerView.Adapter<GuestHolder>() {

    private var guestList: ArrayList<Guest> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): GuestHolder {
        return GuestHolder(LayoutInflater.from(context).inflate(R.layout.guest_list, parent, false))
    }

    override fun getItemCount(): Int = guestList.size

    override fun onBindViewHolder(holder: GuestHolder, position: Int) {
        val fullName = "${guestList[position].firstName} ${guestList[position].lastName}"
        val guestId = "${guestList[position].id}"
        holder.itemView.guestName.text = fullName
        holder.itemView.guestId.text = guestId
        Glide.with(context).load(guestList[position].avatar).into(holder.itemView.guestImg)

        holder.itemView.setOnClickListener{
            Toast.makeText(context,fullName,Toast.LENGTH_SHORT).show()
            context = holder.itemView.context

            val preferences = context.getSharedPreferences("guestName",0)
            val editor = preferences?.edit()
            editor?.putString("guestName", fullName)
            editor?.apply()
            val intent = Intent(context, Main2Activity::class.java)
            context.startActivity(intent)
        }
    }

    fun add(items: List<Guest>) {
        val size = items.size
        for (i in 0 until size) {
            guestList.add(items[i])
        }
        notifyDataSetChanged()
    }
    fun clear(){
        guestList.clear()
    }
}