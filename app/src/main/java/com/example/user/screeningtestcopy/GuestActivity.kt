package com.example.user.screeningtestcopy

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.View
import com.example.user.screeningtestcopy.Model.Guest
import com.example.user.screeningtestcopy.Model.GuestResponse
import com.example.user.screeningtestcopy.service.ApiClient
import com.example.user.screeningtestcopy.service.ApiInterface
import com.jcodecraeer.xrecyclerview.XRecyclerView
import kotlinx.android.synthetic.main.activity_guest.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GuestActivity : AppCompatActivity() {

    lateinit var guestAdapter: GuestAdapter
    private val apiInterface: ApiInterface? = ApiClient.getClient().create(ApiInterface::class.java)

    var page = 1
    var limit = 10

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guest)

        supportActionBar?.title = "Guest"
        initList()
        getGuestList(page)

    }

    private fun initList() {
        guestAdapter = GuestAdapter(this)
        xRecyclerView.layoutManager = GridLayoutManager(applicationContext, 2)
        xRecyclerView.adapter = guestAdapter

        xRecyclerView.setLoadingListener(object : XRecyclerView.LoadingListener {
            override fun onLoadMore() {
                page++
                getGuestList(page)
            }

            override fun onRefresh() {
                page = 1
                getGuestList(page)
            }
        })
    }

    private fun getGuestList(page: Int) {
        apiInterface?.getGuest(page, limit)?.enqueue(object : Callback<GuestResponse<Guest>> {
            override fun onFailure(call: Call<GuestResponse<Guest>>, t: Throwable) {
            }

            override fun onResponse(call: Call<GuestResponse<Guest>>, response: Response<GuestResponse<Guest>>) {
                if (response.isSuccessful) {
                    setData(response.body()?.data)
                }
            }
        })
    }

    private fun setData(guests: List<Guest>?) {
        if (guests != null && guests.isNotEmpty()) {
            if (page == 1) {
                guestAdapter.clear()
                xRecyclerView.removeAllViews()
            }
            guestAdapter.add(guests)
        }
        xRecyclerView.refreshComplete()
        progressBar.visibility = View.GONE
    }
}
